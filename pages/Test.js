import React, { Fragment } from "react";
import Head from "next/head";

import Container from "../components/container";

const Test = (props) => {
  for (let i = 0; (i = 9); i++) {
    return (
      <Container>
        <Head>
          <title>Trivia Challenge</title>
        </Head>

        <div className="card">
          <div className="card-header text-center">
            <h3>{props.data.results[i].category}</h3>
          </div>
          <div className="card-body text-center">
            <h2>{props.data.results[i].question}</h2>
          </div>
          <div className="btn btn-primary text-center p-1 m-2">true</div>
          <div className="btn btn-primary text-center p-1 m-2">false</div>
        </div>
      </Container>
    );
  }
};
Test.getInitialProps = async (ctx) => {
  const res = await fetch(
    "https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean"
  );
  const data = await res.json();

  return { data };
};

export default Test;
