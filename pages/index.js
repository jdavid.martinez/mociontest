import React, { Fragment } from "react";
import Head from "next/head";
import Link from "next/link";

import Container from "../components/container";

const Index = (props) => {
  return (
    <Fragment>
      <Container>
        <Head>
          <title> Mocion Test </title>
        </Head>
        <div className="card text-center">
          <h1>Welcome to the</h1>
          <h1>Trivia Chanllenge!</h1>
          <h4 className="p-4">
            You wil be presented with 10 True or False questions.
          </h4>
          <h2>Can you score 100%?</h2>

          <div className="">
            <button className="btn btn-primary text-center  m-2">
              <Link href="/Test">
                <a className="n">Begin</a>
              </Link>
            </button>
          </div>
        </div>
      </Container>
    </Fragment>
  );
};

export default Index;
